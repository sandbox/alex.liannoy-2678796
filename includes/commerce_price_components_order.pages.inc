<?php

/**
 * @file
 * Page callbacks and required logic for forms.
 */

/**
 * Page/form callback for
 * 'admin/commerce/config/product-pricing/components-order' page.
 */
function commerce_price_components_order_form($form, &$form_state) {
  $form = array();
  $form['#tree'] = TRUE;
  $component_types = module_invoke_all('commerce_price_component_type_info');
  $weights = variable_get('commerce_price_components_order_weight', array());

  foreach ($component_types as $name => $component_type) {
    // Sometimes price components do not have weight at all.
    // Let's set a default a value.
    $component_type['weight'] = 0;
    if (isset($component_type[$name])) {
      $component_type['weight'] = $component_type[$name];
    }
    $weight = $component_type['weight'];
    if (isset($weights[$name])) {
      $weight = $weights[$name];
    }
    $form[$name] = array();
    $form[$name]['name'] = array('#markup' => check_plain($component_type['title']));
    $form[$name]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @title', array('@title' => $component_type['title'])),
      '#title_display' => 'invisible',
      '#delta' => 10,
      '#default_value' => $weight,
    );
    $form[$name]['#weight'] = $weight;
  }

  if (count($component_types) > 1) {
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Order')
    );
  }

  return $form;
}

/**
 * Submit callback for commerce_price_components_order_form form.
 *
 * @see commerce_price_components_order_form()
 */
function commerce_price_components_order_form_submit($form, &$form_state) {
  $to_save = array();

  $component_types = module_invoke_all('commerce_price_component_type_info');
  $allowed = array_keys($component_types);
  foreach ($allowed as $name) {
    if (isset($form_state['values'][$name])) {
      $to_save[$name] = $form_state['values'][$name]['weight'];
    }
  }

  variable_set('commerce_price_components_order_weight', $to_save);
}
