Commerce Price Components Order
===============================

This module provides a user interface allowing to order the price components.
Common use is to reorder taxes, fees and base price on a checkout page.

This module was inspired by case similar with this one:
https://www.drupal.org/node/1473312
